package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

func main() {
	path := flag.String("path", ".", "Project path")
	flag.Parse()

	files, err := Traverse(*path, false)
	if err != nil {
		fmt.Println(err)
	}

	for _, file := range files {
		//Specific cases
		if strings.Index(file, "_test.go") != -1 {
			continue
		}
		if strings.Index(file, ".go") == -1 {
			continue
		}
		if strings.Index(file, "interface.go") != -1 {
			continue
		}
		if strings.Index(file, "main.go") != -1 {
			continue
		}

		fmt.Println(file)

		comm := exec.Command("gofmt", "-w", file)
		out, err := comm.Output()
		if err != nil {
			fmt.Printf("gofmt error: %s\n\n", err)

			continue
		}

		comm = exec.Command("exercism", "submit", "-v", file)
		out, err = comm.Output()
		if err != nil {
			fmt.Printf("exercism submit error: %s\n\n", err)

			continue
		}
		fmt.Printf("exercism submit output: %s\n\n", out)
	}
}

func Traverse(name string, hidden bool) ([]string, error) {
	var out []string

	file, err := os.Open(name)
	if err != nil {
		return nil, err
	}

	stat, err := file.Stat()
	if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		if !hidden && stat.Name()[0] == '.' {
			return nil, nil
		}

		out = append(out, stat.Name())

		return out, nil
	}

	infos, err := ioutil.ReadDir(name)
	if err != nil {
		return nil, err
	}

	for _, info := range infos {
		if !hidden && info.Name()[0] == '.' {
			continue
		}

		path := fmt.Sprintf("%s/%s", name, info.Name())

		if info.IsDir() {
			files, err := Traverse(path, hidden)
			if err != nil {
				return nil, err
			}

			out = append(out, files...)

			continue
		}

		out = append(out, path)
	}

	return out, nil
}
