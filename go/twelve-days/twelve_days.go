package twelve

import (
	"fmt"
	"strings"
)

//Song returns the whole song.
func Song() string {
	var output []string

	for i := 1; i <= 12; i++ {
		output = append(output, Verse(i))
	}

	return strings.Join(output, "\n")
}

//Verse returns a specific verse depending on the day.
func Verse(day int) string {
	return recurse(day, "", "")
}

type day struct {
	Text1 string
	Text2 string
}

//Days hold all the texts for each day.
var Days = []day{
	{
		Text1: "first",
		Text2: "and a Partridge in a Pear Tree",
	},
	{
		Text1: "second",
		Text2: "two Turtle Doves",
	},
	{
		Text1: "third",
		Text2: "three French Hens",
	},
	{
		Text1: "fourth",
		Text2: "four Calling Birds",
	},
	{
		Text1: "fifth",
		Text2: "five Gold Rings",
	},
	{
		Text1: "sixth",
		Text2: "six Geese-a-Laying",
	},
	{
		Text1: "seventh",
		Text2: "seven Swans-a-Swimming",
	},
	{
		Text1: "eighth",
		Text2: "eight Maids-a-Milking",
	},
	{
		Text1: "ninth",
		Text2: "nine Ladies Dancing",
	},
	{
		Text1: "tenth",
		Text2: "ten Lords-a-Leaping",
	},
	{
		Text1: "eleventh",
		Text2: "eleven Pipers Piping",
	},
	{
		Text1: "twelfth",
		Text2: "twelve Drummers Drumming",
	},
}

//recurse takes in as the arguments the final day, the input from previous days,
//and the prefix for the output string. It outputs the final lyrics.
func recurse(day int, input string, prefix string) string {
	if day == 0 {
		return fmt.Sprintf("%s: %s.", prefix, input)
	}

	var out string

	if day == 1 && input == "" {
		out = "a Partridge in a Pear Tree"
	} else {
		out = Days[day-1].Text2
	}

	if prefix == "" {
		prefix = fmt.Sprintf("On the %s day of Christmas my true love gave to me", Days[day-1].Text1)
	}

	if input == "" {
		return recurse(day-1, out, prefix)
	}

	return recurse(day-1, fmt.Sprintf("%s, %s", input, out), prefix)
}
