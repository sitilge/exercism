//Package anagram deals with checking for anagrams.
package anagram

import (
	"strings"
	"unicode/utf8"
)

//Detect which words from a given list are anagrams of a given word, returns
//a slice of the detected words.
func Detect(word string, candidates []string) []string {
	var out []string

	mw := make(map[rune]int)

	word = strings.ToLower(word)

	for _, r := range word {
		mw[r]++
	}

	for i, candidate := range candidates {
		candidate = strings.ToLower(candidate)

		if utf8.RuneCount([]byte(candidate)) != len(word) {
			continue
		}

		if candidate == word {
			continue
		}

		mc := make(map[rune]int)

		for _, r := range candidate {
			mc[r]++
		}

		var found bool

		for _, r := range candidate {
			v1, ok1 := mw[r]
			v2, ok2 := mc[r]

			if !ok1 || !ok2 || v1 != v2 {
				break
			}

			found = true
		}

		if found {
			out = append(out, candidates[i])
		}
	}

	return out
}
