package pangram

import "unicode"

//IsPangram returns if the input string is an anagram.
func IsPangram(input string) bool {
	m := make(map[rune]int)

	for _, r := range input {
		m[unicode.ToUpper(r)]++
	}

	for r := 'A'; r <= 'Z'; r++ {
		_, ok := m[r]
		if !ok {
			return false
		}
	}

	return true
}
