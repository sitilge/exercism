//Package acronym both abbreviates and sanitezes strings.
package acronym

import (
	"regexp"
	"strings"
)

//Accumulate takes as an input a string and returns the abbreviation of the string.
func Abbreviate(s string) string {
	s = SanitizeString(s)

	words := strings.Split(strings.ToUpper(s), " ")

	var letters []uint8

	for _, word := range words {
		letter := word[0]

		letters = append(letters, letter)
	}

	return string(letters)
}

//SanitizeString takes as an input a string and returns a string with no whitespace, dash or underscore.
func SanitizeString(s string) string {
	s = regexp.MustCompile(`[\s-_]+`).ReplaceAllString(s, " ")

	return s
}
