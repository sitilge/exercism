// Package tree implements utils for building trees.
package tree

import (
	"fmt"
	"sort"
)

// Record represents record loaded from DB.
type Record struct {
	ID     int
	Parent int
}

//Node represents tree node.
type Node struct {
	ID       int
	Children []*Node
}

type records []Record

type byID struct {
	records
}

// Build creates a tree from provided records
// and returns pointer to its root node.
func Build(records []Record) (*Node, error) {
	sort.Sort(byID{records})

	//Error checking the root node.
	switch {
	case len(records) == 0:
		return nil, nil
	case records[0].ID != 0:
		return nil, fmt.Errorf("no root node")
	case records[0].Parent != 0:
		return nil, fmt.Errorf("root node has parent")
	}

	nodes := make([]*Node, len(records))

	for i, record := range records {
		//Error checking on non-root nodes.
		switch {
		case record.ID != i:
			return nil, fmt.Errorf("duplicate or non-continous node")
		case record.ID < record.Parent:
			return nil, fmt.Errorf("root node has parent")
		case record.ID != 0 && record.ID == record.Parent:
			return nil, fmt.Errorf("cycle directly")
		}

		node := &Node{
			ID: record.ID,
		}

		nodes[record.ID] = node

		if record.ID == 0 {
			continue
		}

		nodes[record.Parent].Children = append(nodes[record.Parent].Children, nodes[record.ID])
	}

	return nodes[0], nil
}

//Returns the number of records.
func (inputs byID) Len() int {
	return len(inputs.records)
}

//Returns true if the current record ID is less than the next record ID.
func (inputs byID) Less(i int, j int) bool {
	return inputs.records[i].ID < inputs.records[j].ID
}

//Swap records i and j.
func (inputs byID) Swap(i int, j int) {
	inputs.records[i], inputs.records[j] = inputs.records[j], inputs.records[i]
}
