package proverb

import "fmt"

//Proverb outputs a funny sentence.
func Proverb(rhyme []string) []string {
	var out []string

	for i := range rhyme {
		if i+1 < len(rhyme) {
			out = append(out, fmt.Sprintf("For want of a %s the %s was lost.", rhyme[i], rhyme[i+1]))
		} else {
			out = append(out, fmt.Sprintf("And all for the want of a %s.", rhyme[0]))
		}
	}

	return out
}
