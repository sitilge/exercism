package encode

import (
	"fmt"
	"strconv"
	"strings"
)

//TODO - I assume only ASCII encoding...

//RunLengthEncode encodes the given input.
func RunLengthEncode(input string) string {
	var out []string

	for i := 0; i < len(input); i++ {
		count := 0

		for j := i; j < len(input); j++ {
			if input[i] != input[j] {
				break
			}

			count++
		}

		if count > 1 {
			out = append(out, strconv.Itoa(count))
		}

		out = append(out, string(input[i]))

		i += count - 1
	}

	return strings.Join(out, "")
}

//RunLengthDecode decodes the given input.
func RunLengthDecode(input string) string {
	var out []string

	var digits []string

	for i := 0; i < len(input); i++ {
		count := 0

		_, err := strconv.Atoi(string(input[i]))
		if err != nil {
			i += count

			number, _ := strconv.Atoi(strings.Join(digits, ""))

			if number > 1 {
				out = append(out, fmt.Sprintf("%s", strings.Repeat(string(input[i]), number)))
			} else {
				out = append(out, fmt.Sprintf("%s", string(input[i])))
			}

			digits = nil

			continue
		}

		count++

		digits = append(digits, string(input[i]))
	}

	return strings.Join(out, "")
}
