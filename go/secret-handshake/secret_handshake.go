package secret

import (
	"fmt"
	"log"
	"sort"
	"strconv"
)

type reverse struct {
	items []string
}

//Len returns number of elements.
func (r reverse) Len() int {
	return len(r.items)
}

//Less returns if first element is less than second element.
func (r reverse) Less(i, j int) bool {
	return true
}

//Swap swaps elements.
func (r reverse) Swap(i, j int) {
	r.items[i], r.items[j] = r.items[j], r.items[i]
}

//Handshake returns secret handshake.
func Handshake(input uint) []string {
	var output []string

	binary, err := strconv.Atoi(fmt.Sprintf("%b", input))
	if err != nil {
		log.Fatal(err)
	}

	if binary%2 == 1 {
		output = append(output, "wink")
	}

	if (binary/10)%2 == 1 {
		output = append(output, "double blink")
	}

	if (binary/100)%2 == 1 {
		output = append(output, "close your eyes")
	}

	if (binary/1000)%2 == 1 {
		output = append(output, "jump")
	}

	if (binary/10000)%2 == 1 {
		sort.Sort(reverse{output})
	}

	return output
}
