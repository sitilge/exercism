package robotname

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//Robot struct holds the model of the robot.
type Robot struct {
	Model string
}

//MutexMap is used for concurrent map access.
type MutexMap struct {
	sync.RWMutex
	Map map[string]int
}

//Names hold all the names and the number of names.
var Names MutexMap

func init() {
	Names.Map = make(map[string]int)
	rand.Seed(time.Now().Unix())
}

//Get a new robot name.
func (robot *Robot) Name() (string, error) {
	if robot.Model != "" {
		return robot.Model, nil
	}

	errs := make(chan error)
	names := make(chan string)

	go func() {
		name, err := Names.RandomName()
		if err != nil {
			errs <- err

			return
		}

		Names.Lock()
		Names.Map[name]++
		Names.Unlock()

		robot.Model = name

		names <- name
	}()

	select {
	case name := <-names:
		return name, nil
	case err := <-errs:
		fmt.Println("No, error")
		return "", err
	}
}

//Generate a new random name for robot.
func (mm *MutexMap) RandomName() (string, error) {
	for {
		mm.RLock()
		count := len(mm.Map)
		mm.RUnlock()

		if count >= 26*26*1000 {
			return "", fmt.Errorf("name pool exausted")
		}

		name := fmt.Sprintf("%c%c%03v", 'A'+rand.Intn(26), 'A'+rand.Intn(26), rand.Intn(1000))

		_, existing := mm.Map[name]

		if !existing {
			return name, nil
		}
	}
}

// Reset generate a new name for Robot.
func (robot *Robot) Reset() {
	robot.Model = ""
}
