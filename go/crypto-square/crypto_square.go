package cryptosquare

import (
	"math"
	"strings"
	"unicode"
)

//Encode encodes the input string according to the crypto square.
func Encode(input string) string {
	var letters []rune

	for _, letter := range input {
		if letter >= '0' && letter <= '9' {
			letters = append(letters, letter)
		} else if (letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z') {
			letters = append(letters, unicode.ToLower(letter))
		}
	}

	r := int(math.Floor(math.Sqrt(float64(len(letters)))))
	c := r + 1

	if r*r == len(letters) {
		c = r
	} else if c*r < len(letters) {
		r++
	}

	var rows []string

	var pad bool
	if c*r > len(letters) {
		pad = true
	}

	for i := 0; i < c; i++ {
		var output []rune

		for j := 0; j < r; j++ {
			if i+j*c >= len(letters) {
				if pad {
					output = append(output, ' ')
				}

				continue
			}

			output = append(output, letters[i+j*c])
		}

		rows = append(rows, string(output))
	}

	return strings.Join(rows, " ")
}
