package erratum

import (
	"fmt"
)

//Use is opening a new resource, handling errors and panics, and gracefully closing the resource.
func Use(o ResourceOpener, input string) (output error) {
	resource, err := o()
	//Continue on transient errors.
	for err != nil {
		_, ok := err.(TransientError)
		if !ok {
			return err
		}

		resource, err = o()
	}
	defer resource.Close()

	defer func() {
		if r := recover(); r != nil {
			switch r.(type) {
			case FrobError:
				resource.Defrob(r.(FrobError).defrobTag)
				output = r.(error)
			case error:
				output = r.(error)
			default:
				output = fmt.Errorf("%v", r)
			}
		}
	}()

	resource.Frob(input)

	return output
}
