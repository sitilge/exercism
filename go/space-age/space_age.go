//Package deals with planet names and ages.
package space

// Planet is a string alias for a planet's name.
type Planet string

// Age converts the given seconds to years of the given planed.
func Age(seconds float64, planet Planet) float64 {
	var earth float64 = 31557600
	var age float64

	switch planet {
	case "Mercury":
		age = seconds / (0.2408467 * earth)
	case "Venus":
		age = seconds / (0.61519726 * earth)
	case "Earth":
		age = seconds / (1 * earth)
	case "Mars":
		age = seconds / (1.8808158 * earth)
	case "Jupiter":
		age = seconds / (11.862615 * earth)
	case "Saturn":
		age = seconds / (29.447498 * earth)
	case "Uranus":
		age = seconds / (84.016846 * earth)
	case "Neptune":
		age = seconds / (164.79132 * earth)
	}

	return age
}
