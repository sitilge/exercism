package pascal

//Triangle return the whole Pascal's triangle, up to a certain level.
func Triangle(level int) [][]int {
	var out [][]int

	for i := 1; i <= level; i++ {
		out = append(out, Row(i))
	}

	return out
}

//Row returns the integers of Pascal's triangle in a specific row.
func Row(level int) []int {
	var triangle, out []int

	if level > 2 {
		triangle = Row(level - 1)
	}

	for i := 1; i <= level; i++ {
		if i == 1 || i == level {
			out = append(out, 1)

			continue
		}

		out = append(out, triangle[i-1]+triangle[i-1-1])
	}

	return out
}
