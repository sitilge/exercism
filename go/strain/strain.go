package strain

type Ints []int

type Strings []string

type Lists []Ints

func (i Ints) Keep(f func(input int) bool) Ints {
	var output Ints

	for _, num := range i {
		if f(num) {
			output = append(output, num)
		}
	}

	return output
}

func (i Ints) Discard(f func(input int) bool) Ints {
	var output Ints

	for _, num := range i {
		if !f(num) {
			output = append(output, num)
		}
	}

	return output
}

func (s Strings) Keep(f func(input string) bool) Strings {
	var output Strings

	for _, str := range s {
		if f(str) {
			output = append(output, str)
		}
	}

	return output
}

func (s Strings) Discard(f func(input string) bool) Strings {
	var output Strings

	for _, str := range s {
		if !f(str) {
			output = append(output, str)
		}
	}

	return output
}

func (l Lists) Keep(f func(input []int) bool) Lists {
	var output Lists

	for _, lis := range l {
		if f(lis) {
			output = append(output, lis)
		}
	}

	return output
}

func (l Lists) Discard(f func(input []int) bool) Lists {
	var output Lists

	for _, lis := range l {
		if !f(lis) {
			output = append(output, lis)
		}
	}

	return output
}
