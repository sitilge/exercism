package diffsquares

//SquareOfSum returns the square of the input sums.
func SquareOfSum(input int) int {
	//Some math.
	sum := input * (input + 1) / 2

	return sum * sum
}

//SumOfSquares returns the sum of the input squares.
func SumOfSquares(input int) int {
	var sum int

	for i := 0; i <= input; i++ {
		sum += i * i
	}

	return sum
}

//Difference returns the difference.
func Difference(input int) int {
	return SquareOfSum(input) - SumOfSquares(input)
}
