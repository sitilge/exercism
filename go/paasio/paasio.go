// Package paasio implements reporting network IO statistics.
package paasio

import (
	"io"
	"sync"
)

type counter struct {
	sync.Mutex
	r      func(p []byte) (int, error)
	w      func(p []byte) (int, error)
	nops   int
	totalR int64
	totalW int64
}

//Read reads the input byte slice and returns the current reads.
func (c *counter) Read(p []byte) (int, error) {
	c.Lock()
	defer c.Unlock()

	n, err := c.r(p)
	if err != nil {
		return 0, err
	}

	c.nops++
	c.totalR += int64(n)

	return n, nil
}

//ReadCount returns the current reads.
func (c *counter) ReadCount() (int64, int) {
	return c.totalR, c.nops
}

//Write writes the input byte slice and returns the current writes.
func (c *counter) Write(p []byte) (int, error) {
	c.Lock()
	defer c.Unlock()

	n, err := c.w(p)
	if err != nil {
		return 0, err
	}

	c.nops++
	c.totalW += int64(n)

	return n, nil
}

//WriteCount returns the write count.
func (c *counter) WriteCount() (int64, int) {
	return c.totalW, c.nops
}

// NewReadWriteCounter returns new read and write struct.
func NewReadWriteCounter(rw io.ReadWriter) ReadWriteCounter {
	return &counter{
		r: rw.Read,
		w: rw.Write,
	}
}

// NewReadCounter returns new read struct.
func NewReadCounter(r io.Reader) ReadCounter {
	return &counter{
		r: r.Read,
	}
}

// NewWriteCounter returns new write struct.
func NewWriteCounter(w io.Writer) WriteCounter {
	return &counter{
		w: w.Write,
	}
}
