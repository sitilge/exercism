package pythagorean

import (
	"sort"
)

// Triplet is a pythagorean triplet with sides a < b < c.
type Triplet [3]int

type byLength struct {
	T []Triplet
}

//Len returns number of elements.
func (b byLength) Len() int {
	return len(b.T)
}

//Less if elements are sorted increasingly.
func (b byLength) Less(i, j int) bool {
	if b.T[i][0] < b.T[j][0] {
		return true
	} else if b.T[i][0] == b.T[j][0] {
		if b.T[i][1] < b.T[j][1] {
			return true
		} else if b.T[i][1] == b.T[j][1] {
			if b.T[i][2] < b.T[j][2] {
				return true
			}
		}
	}

	return false
}

//Swap two elements.
func (b byLength) Swap(i, j int) {
	b.T[i], b.T[j] = b.T[j], b.T[i]
}

// Range returns a list of all Pythagorean triplets with sides in the
// range min to max inclusive.
func Range(min, max int) []Triplet {
	var out []Triplet
	m := make(map[int]int)

	for i := min; i <= max; i++ {
		m[i] = i * i
	}

	//TODO - sad, sad On3
	for i, vi := range m {
		for j, vj := range m {
			if i >= j {
				continue
			}

			for k, vk := range m {
				if j >= k {
					continue
				}

				if vi+vj == vk {
					out = append(out, Triplet{i, j, k})
				}
			}
		}
	}

	sort.Sort(byLength{out})

	return out
}

// Sum returns a list of all Pythagorean triplets where the sum a+b+c
// (the perimeter) is equal to p.
func Sum(num int) []Triplet {
	var out []Triplet

	//TODO - hmm, interesting... How to pick the num?
	triplets := Range(1, num)

	for _, triplet := range triplets {
		if triplet[0]+triplet[1]+triplet[2] == num {
			out = append(out, triplet)
		}
	}

	return out
}
