// Package leap solves Exercism Leap.
package leap

//IsLeapYear determines whether a provided year was a leap year,
//according to the Gregorian calendar.
func IsLeapYear(year int) bool {
	if year%4 != 0 {
		return false
	}

	if year%100 == 0 && year%400 != 0 {
		return false
	}

	return true
}
