package strand

//ToRNA builds the respective RNAaa, translation.
func ToRNA(dna string) string {
	var out []rune

	for _, r := range dna {
		switch r {
		case 'G':
			out = append(out, 'C')
		case 'C':
			out = append(out, 'G')
		case 'T':
			out = append(out, 'A')
		case 'A':
			out = append(out, 'U')
		}
	}

	return string(out)
}
