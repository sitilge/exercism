//Package grains deals with chess-like game operations.
package grains

import (
	"fmt"
)

//Square function does 2^input operation.
func Square(input int) (uint64, error) {
	if input < 1 || input > 64 {
		return 0, fmt.Errorf("bad square %d (must be 1-64)", input)
	}

	return 1 << (input - 1), nil
}

//Total outputs the total number of grains on the chessboard.
func Total() uint64 {
	return 1<<64 - 1
}
