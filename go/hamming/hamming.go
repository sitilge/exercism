package hamming

import (
	"errors"
)

//Distance returns the hamming distance between two integers.
func Distance(a, b string) (int, error) {
	ra := []rune(a)
	rb := []rune(b)

	if len(ra) != len(rb) {
		return 0, errors.New("strands are not the same length")
	}

	var distance int

	for i := range ra {
		if ra[i] != rb[i] {
			distance++
		}
	}

	return distance, nil
}
