//Package account handles atomic operations on the bank account.
package account

import (
	"sync"
)

//Account struct holds your account information and a mutex.
type Account struct {
	sync.RWMutex
	Money  int
	Closed bool
}

//Open new bank account.
func Open(money int) *Account {
	if money < 0 {
		return nil
	}

	return &Account{
		Money: money,
	}
}

//Close the bank account and return the money left and transaction success status.
func (acc *Account) Close() (int, bool) {
	acc.Lock()
	defer acc.Unlock()

	if acc.Closed {
		return 0, false
	}

	acc.Closed = true

	return acc.Money, true
}

//Get the current bank account balance and transaction success status.
func (acc *Account) Balance() (int, bool) {
	acc.Lock()
	defer acc.Unlock()

	if acc.Closed {
		return 0, false
	}

	return acc.Money, true
}

//Deposit money and return the current balance and transaction success status.
func (acc *Account) Deposit(money int) (int, bool) {
	acc.Lock()
	defer acc.Unlock()

	if acc.Closed {
		return 0, false
	}

	if acc.Money+money < 0 {
		return 0, false
	}

	acc.Money += money

	return acc.Money, true
}
