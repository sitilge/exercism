//Package gigasecond handles gigasecond operations.
package gigasecond

import (
	"math"
	"time"
)

//AddGigasecond adds a gigasecond to a given time.
func AddGigasecond(t time.Time) time.Time {
	long := int(math.Pow(10, 9+9))
	duration := time.Duration(long)

	return t.Add(duration)
}
