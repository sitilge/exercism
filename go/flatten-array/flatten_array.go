//Package flatten recursively flattens an interface.
package flatten

//Flatten takes any interface and outputs a flat slice of interfaces.
func Flatten(input interface{}) []interface{} {
	output := make([]interface{}, 0)
	r := input.([]interface{})

	for _, v := range r {
		if v == nil {
			continue
		}

		switch v.(type) {
		case []interface{}:
			for _, f := range Flatten(v) {
				if f == nil {
					continue
				}

				output = append(output, f)
			}
		default:
			output = append(output, v)
		}
	}

	return output
}
