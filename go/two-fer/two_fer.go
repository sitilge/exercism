package twofer

import "fmt"

//ShareWith outputs some funny sentence.
func ShareWith(name string) string {
	placeholder := "One for %s, one for me."

	if name == "" {
		return fmt.Sprintf(placeholder, "you")
	}

	return fmt.Sprintf(placeholder, name)
}
