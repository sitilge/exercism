package triangle

import (
	"math"
	"sort"
)

// Notice KindFromSides() returns this type. Pick a suitable data type.
type Kind int

const (
	NaT = 0 // not a triangle
	Equ = 1 // equilateral
	Iso = 2 // isosceles
	Sca = 3 // scalene
)

type sides struct {
	s []float64
}

//Len return number of elements.
func (s sides) Len() int {
	return len(s.s)
}

//Less returns of current element value is less than next element value.
func (s sides) Less(i, j int) bool {
	return s.s[i] < s.s[j]
}

//Swap swaps two elements.
func (s sides) Swap(i, j int) {
	s.s[i], s.s[j] = s.s[j], s.s[i]
}

//KindFromSides checks if inputs are kind from each side.
func KindFromSides(a, b, c float64) Kind {
	if math.IsNaN(a) || math.IsNaN(b) || math.IsNaN(c) {
		return NaT
	}

	if math.IsInf(a, 0) || math.IsInf(b, 0) || math.IsInf(c, 0) {
		return NaT
	}

	if a <= 0 || b <= 0 || c <= 0 {
		return NaT
	}

	s := []float64{a, b, c}
	sort.Sort(sides{s})

	if s[0]+s[1] < s[2] {
		return NaT
	}

	if s[0] == s[1] && s[1] == s[2] {
		return Equ
	}

	if s[0] == s[1] || s[1] == s[2] {
		return Iso
	}

	return Sca
}
