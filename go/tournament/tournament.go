package tournament

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"
)

//Team holds all information about a specific team.
type Team struct {
	Name    string
	Matches int
	Wins    int
	Draws   int
	Losses  int
	Points  int
}

//ByPointsAlpha allows to sort by points alphanumerically.
type ByPointsAlpha struct {
	Sortable []*Team
}

// Tally read scores and display the tournament results.
func Tally(reader io.Reader, writer io.Writer) error {
	tallyMap := make(map[string]*Team)

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" || line[0] == '#' {
			continue
		}

		split := strings.Split(line, ";")
		if len(split) != 3 {
			return fmt.Errorf("invalid input, must be formated e.g. 'teamA;teamB;teamA result WRT teamB'")
		}

		teamA, teamB, outcome := split[0], split[1], split[2]

		_, ok1 := tallyMap[teamA]

		if !ok1 {
			tallyMap[teamA] = &Team{
				Name: teamA,
			}
		}

		_, ok2 := tallyMap[teamB]

		if !ok2 {
			tallyMap[teamB] = &Team{
				Name: teamB,
			}
		}

		tallyMap[teamA].Matches++
		tallyMap[teamB].Matches++

		switch outcome {
		case "win":
			tallyMap[teamA].Wins++
			tallyMap[teamA].Points += 3
			tallyMap[teamB].Losses++
		case "draw":
			tallyMap[teamA].Draws++
			tallyMap[teamA].Points++
			tallyMap[teamB].Draws++
			tallyMap[teamB].Points++
		case "loss":
			tallyMap[teamA].Losses++
			tallyMap[teamB].Wins++
			tallyMap[teamB].Points += 3
		default:
			return fmt.Errorf("uknown action: %s", outcome)
		}
	}

	//Do sorting.
	var teams []*Team
	for _, team := range tallyMap {
		teams = append(teams, team)
	}

	tally := ByPointsAlpha{teams}.Sortable

	//TODO - remember this approach!
	sort.Slice(tally, func(i, j int) bool {
		if tally[i].Points == tally[j].Points {
			return tally[i].Name < tally[j].Name
		}
		return tally[i].Points > tally[j].Points
	})

	format := "%-31s|%3v |%3v |%3v |%3v |%3v\n"
	_, err := fmt.Fprintf(writer, fmt.Sprintf(format, "Team", "MP", "W", "D", "L", "P"))
	if err != nil {
		log.Fatal(err)
	}

	for _, team := range teams {
		_, err := fmt.Fprintf(writer, fmt.Sprintf(format, team.Name, team.Matches, team.Wins, team.Draws, team.Losses, team.Points))
		if err != nil {
			log.Fatal(err)
		}
	}

	return nil
}
