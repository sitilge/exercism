// Package luhn is a simple checktotal formula used to validate a variety of identification numbers, such as credit card numbers and Canadian Social Insurance Numbers.
package luhn

import (
	"strconv"
	"strings"
)

// Valid checks whether card number is valid or not.
func Valid(input string) bool {
	input = strings.ReplaceAll(input, " ", "")

	if len(input) < 2 {
		return false
	}

	var sum int
	var double bool

	for i := len(input) - 1; i >= 0; i-- {
		digit, err := strconv.Atoi(string(input[i]))
		if err != nil {
			return false
		}

		if double {
			digit *= 2

			if digit > 9 {
				digit -= 9
			}
		}

		double = !double

		sum += digit
	}

	return sum%10 == 0
}
