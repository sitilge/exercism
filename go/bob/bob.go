//Package bob manages the conversation with a person.
package bob

import (
	"strings"
)

//Hey takes a string as an input and outputs a response depending on punctuation in the string.
func Hey(remark string) string {
	remark = strings.Trim(remark, " \t\n\r")

	if remark == "" {
		return "Fine. Be that way!"
	}

	if remark[len(remark)-1] == '?' {
		if remark == strings.ToUpper(remark) && ContainsLetter(remark) {
			return "Calm down, I know what I'm doing!"
		}

		return "Sure."
	}

	if remark == strings.ToUpper(remark) && ContainsLetter(remark) {
		return "Whoa, chill out!"
	}

	return "Whatever."
}

//ContainsLetter checks if a string contains a letter.
func ContainsLetter(remark string) bool {
	for _, letter := range remark {
		if letter >= 'a' && letter <= 'z' || letter >= 'A' && letter <= 'Z' {
			return true
		}
	}

	return false
}
