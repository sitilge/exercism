// Package letter counts the frequency of letters in texts
package letter

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// ConcurrentFrequency counts concurrently the frequency of each rune in a given
// texts and returns this data as FreqMap.
func ConcurrentFrequency(list []string) FreqMap {
	c := make(chan FreqMap, 1)

	for _, s := range list {
		go func(s string) {
			c <- Frequency(s)
		}(s)
	}

	m := FreqMap{}
	for range list {
		for k, v := range <-c {
			m[k] += v
		}
	}

	return m
}
