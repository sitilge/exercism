// Package matrix implements a solution of the exercise titled `Matrix'.
package matrix

import (
	"errors"
	"strconv"
	"strings"
)

//Matrix holds rows and columns.
type Matrix struct {
	Row [][]int
	Col [][]int
}

//New returns a matrix containing values from string
//Returns error on bad formatted strings.
func New(s string) (*Matrix, error) {
	m := &Matrix{}

	rows := strings.Split(s, "\n")

	for i, row := range rows {
		row = strings.Trim(row, " ")
		columns := strings.Split(row, " ")

		//Can optimize and bring row checking here.
		m.Row = append(m.Row, nil)

		for j, column := range columns {
			element, err := strconv.Atoi(column)
			if err != nil {
				return nil, err
			}

			m.Col = append(m.Col, nil)
			m.Col[j] = append(m.Col[j], element)
			m.Row[i] = append(m.Row[i], element)
		}

		//Remove excess columns
		m.Col = m.Col[:len(columns)]
	}

	//Do error checking when we know all rows and columns
	for i := range m.Row {
		if i > 0 {
			if len(m.Row[i]) != len(m.Row[i-1]) {
				return nil, errors.New("not a valid matrix")
			}
		}
	}
	for j := range m.Col {
		if j > 0 {
			if len(m.Col[j]) != len(m.Col[j-1]) {
				return nil, errors.New("not a valid matrix")
			}
		}
	}

	return m, nil
}

// Set sets the value of specified row and column of Matrix.
func (m *Matrix) Set(i int, j int, value int) bool {
	if i < 0 || i >= len(m.Row) || j < 0 || j >= len(m.Col) {
		return false
	}

	m.Row[i][j] = value
	m.Col[j][i] = value

	return true
}

// Rows returns the list of Matrix's rows.
func (m *Matrix) Rows() [][]int {
	var out [][]int

	for _, row := range m.Row {
		r := make([]int, len(row))
		copy(r, row)

		out = append(out, r)
	}

	return out
}

// Cols returns the list of Matrix's columns.
func (m *Matrix) Cols() [][]int {
	var out [][]int

	for _, col := range m.Col {
		r := make([]int, len(col))
		copy(r, col)

		out = append(out, r)
	}

	return out
}
