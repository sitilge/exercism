//Package accumulate deals with data conversion.
package accumulate

//Accumulate takes as an input a slice of strings, applies converter on the input and returns
//the converted result.
func Accumulate(inputs []string, converter func(string) string) []string {
	var outputs []string

	for _, input := range inputs {
		outputs = append(outputs, converter(input))
	}

	return outputs
}
