package romannumerals

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

//ToRomanNumeral transforms an arabic number to Roman number.
func ToRomanNumeral(arabic int) (string, error) {
	if arabic <= 0 || arabic > 3000 {
		return "", fmt.Errorf("only numbers [0, 3000] are supported")
	}

	var roman []string

	s := strconv.Itoa(arabic)

	for i, r := range s {
		if r == 0 {
			continue
		}

		level := len(s) - i

		count, err := strconv.Atoi(string(r))
		if err != nil {
			log.Fatal(err)
		}

		var s string

		switch level {
		case 1:
			s = translate(count, "I", "V", "X")
		case 2:
			s = translate(count, "X", "L", "C")
		case 3:
			s = translate(count, "C", "D", "M")
		case 4:
			//Since we have 3000 limit, we don't have to thing about anything else here
			s = translate(count, "M", "", "")
		}

		roman = append(roman, s)
	}

	return strings.Join(roman, ""), nil
}

func translate(count int, current string, next1 string, next2 string) string {
	switch {
	case count == 1 || count == 2 || count == 3:
		return strings.Repeat(current, count)
	case count == 4:
		return fmt.Sprintf("%s%s", current, next1)
	case count == 5:
		return fmt.Sprintf("%s", next1)
	case count == 6 || count == 7 || count == 8:
		return fmt.Sprintf("%s%s", next1, strings.Repeat(current, count-5))
	case count == 9:
		return fmt.Sprintf("%s%s", current, next2)
	}

	return ""
}
