//Package clock contains function for clock management.
package clock

import (
	"fmt"
)

//Clock contains current hour and minute.
type Clock struct {
	minute int
}

//New returns a new clock object.
func New(hour int, minute int) Clock {
	//Wrapping around days.
	minutes := (hour*60 + minute) % 1440

	if minutes < 0 {
		minutes += 1440
	}

	return Clock{
		minute: minutes,
	}
}

//Add adds minutes to the clock.
func (clock Clock) Add(minute int) Clock {
	return New(clock.minute/60, clock.minute%60+minute)
}

//Subtract subtracts minutes from the clock.
func (clock Clock) Subtract(minute int) Clock {
	return New(clock.minute/60, clock.minute%60-minute)
}

//String outputs the clock string.
func (clock Clock) String() string {
	return fmt.Sprintf("%02v:%02v", clock.minute/60, clock.minute%60)
}
