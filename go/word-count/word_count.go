package wordcount

import (
	"regexp"
	"strings"
)

//Frequency holds the frequency map of words.
type Frequency map[string]int

//WordCount returns a map of appearances of each word.
func WordCount(input string) Frequency {
	output := make(Frequency)

	var formated string
	unallowedGeneral := regexp.MustCompile(`[^0-9a-zA-Z']+`)
	unallowedTrailing := regexp.MustCompile(`^[\W]+|\W[\s]|[\s]\W|[\W]+$`)
	whiteTrailing := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	whiteDouble := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	formated = unallowedGeneral.ReplaceAllString(input, " ")
	formated = unallowedTrailing.ReplaceAllString(formated, " ")
	formated = whiteDouble.ReplaceAllString(formated, " ")
	formated = whiteTrailing.ReplaceAllString(formated, "")

	words := strings.Split(formated, " ")

	for _, word := range words {
		output[strings.ToLower(word)]++
	}

	return output
}
