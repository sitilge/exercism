// Package reverse implements functionality for reversing strings.
package reverse

// Reverse reverses a given string.
func Reverse(input string) string {
	var runes []rune
	var output []rune

	var n int

	//Careful here, do not use len for iteration. Because of
	//the possible UTF encoded strings, the results by len
	//and range are not equal.
	for _, r := range input {
		runes = append(runes, r)
		n++
	}

	if runes == nil {
		return ""
	}

	for i := n - 1; i >= 0; i-- {
		output = append(output, runes[i])
	}

	return string(output)
}
