package sublist

import (
	"strconv"
	"strings"
)

//Relation represents the relationship between lists.
type Relation string

//Sublist returns a string representing the grouping of lists.
func Sublist(ra []int, rb []int) Relation {
	var inverse bool
	if len(ra) < len(rb) {
		inverse = true

		ra, rb = rb, ra
	}

	var ca, cb []string
	for i := range ra {
		ca = append(ca, strconv.Itoa(ra[i]))
	}
	for i := range rb {
		cb = append(cb, strconv.Itoa(rb[i]))
	}
	sa := strings.Join(ca, ",")
	sb := strings.Join(cb, ",")

	if sa == sb {
		return "equal"
	} else if strings.Index(sa, sb) < 0 {
		return "unequal"
	} else {
		if inverse == true {
			return "sublist"
		}

		return "superlist"
	}
}
