package protein

//import "errors"
//
//type ErrStop struct{
//	Name string
//}
//
//func (e *ErrStop) Error() string {
//	return e.Name + ": stop"
//}
//
//type ErrInvalidBase struct{
//	Name string
//}
//
//func (e *ErrInvalidBase) Error() string {
//	return e.Name + ": invalid base"
//}

var ErrStop error
var ErrInvalidBase error

func FromCodon(s string) (string, error) {
	switch {
	case s == "AUG":
		return "Methionine", nil
	case s == "UUU" || s == "UUC":
		return "Phenylalanine", nil
	case s == "UUA" || s == "UUG":
		return "Leucine", nil
	case s == "UCU" || s == "UCC" || s == "UCA" || s == "UCG":
		return "Serine", nil
	case s == "UAU" || s == "UAC":
		return "Tyrosine", nil
	case s == "UGU" || s == "UGC":
		return "Cysteine", nil
	case s == "UGG":
		return "Tryptophan", nil
	case s == "UAA" || s == "UAG" || s == "UGA":
		//return "", &ErrStop{}
	default:
		//return "", &ErrInvalidBase{}
	}
}

func FromRNA(s string) ([]string, error) {
	var output []string

	var buffer []rune

	for _, letter := range s {
		buffer = append(buffer, letter)
		//fmt.Println(len(buffer), string(letter))
		if len(buffer)%3 == 0 {
			result, err := FromCodon(string(buffer))

			//Error type assertion
			//eStop, ok := err.(*ErrStop)
			//if ok {
			//	fmt.Println(eStop)
			//
			//	return output, nil
			//}

			output = append(output, result)

			//Error type assertion
			//eInvalidBase, ok := err.(*ErrStop)
			//if ok {
			//	fmt.Println(eInvalidBase)
			//
			//	return nil, eInvalidBase
			//}

			buffer = nil
		}
	}

	return output, nil
}
