package lsproduct

import (
	"errors"
	"strconv"
)

//LargestSeriesProduct finds the span that produces the largest multiple.
func LargestSeriesProduct(digits string, span int) (int, error) {
	if span < 0 || len(digits) < span {
		return 0, errors.New("incorrect digits/span length")
	}

	if len(digits) == 0 {
		return 1, nil
	}

	max := 0

	for i := range digits {
		multiple := 1

		if i < span-1 {
			continue
		}

		limited := digits[i-span+1 : i+1]

		for _, l := range limited {
			digit, err := strconv.Atoi(string(l))
			if err != nil {
				return 0, err
			}

			multiple *= digit
		}

		if multiple > max {
			max = multiple
		}
	}

	return max, nil
}
