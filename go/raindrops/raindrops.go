// Package raindrops contains a utility function to create a raindrop string based on a number.
package raindrops

import (
	"strconv"
	"strings"
)

// Convert returns raindrop string based on the given number.
func Convert(input int) string {
	var output []string

	if input%3 == 0 {
		output = append(output, "Pling")
	}

	if input%5 == 0 {
		output = append(output, "Plang")
	}

	if input%7 == 0 {
		output = append(output, "Plong")
	}

	if output == nil {
		return strconv.Itoa(input)
	}

	return strings.Join(output, "")
}
