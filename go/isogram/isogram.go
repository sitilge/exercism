package isogram

import "strings"

//IsIsogram checks if the input string is an isogram.
func IsIsogram(input string) bool {
	m := make(map[rune]int, 0)

	input = strings.ToLower(input)

	for _, letter := range input {
		_, ok := m[letter]

		if ok {
			return false
		}

		if letter >= 'a' && letter <= 'z' {
			m[letter]++
		}
	}

	return true
}
