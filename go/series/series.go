package series

// All returns all series of consecutive numbers of lenght n from string s.
func All(n int, s string) []string {
	var out []string

	runes := []rune(s)

	for i := range runes {
		if i+n > len(runes) {
			break
		}

		out = append(out, string(runes[i:i+n]))
	}

	return out
}

// UnsafeFirst returns first serie of consecutive numbers of lenght n from string s.
func UnsafeFirst(n int, s string) string {
	all := All(n, s)

	return all[0]
}

// First returns first serie of consecutive numbers of lenght n from string s.
// Returns false if it can't find any.
func First(n int, s string) (string, bool) {
	if n > len(s) {
		return "", false
	}

	all := All(n, s)

	return all[0], true
}
