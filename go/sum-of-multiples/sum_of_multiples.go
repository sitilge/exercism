package summultiples

//SumMultiples returns the sum of all unique multiples of a number.
func SumMultiples(limit int, multiples ...int) int {
	var out int
	m := make(map[int]bool)

	for _, multiple := range multiples {
		if multiple <= 0 {
			continue
		}

		for i := 1; i*multiple < limit; i++ {
			digit := i * multiple

			_, ok := m[digit]
			if !ok {
				m[digit] = true
				out += digit
			}
		}
	}

	return out
}
