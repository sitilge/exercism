package prime

import (
	"fmt"
	"math"
)

var m map[int]bool

//Nth finds the nth prime.
func Nth(n int) (int, bool) {
	m = make(map[int]bool, math.MaxInt16)

	if n == 1 {
		return 2, true
	}

	for i := 1; i <= math.MaxInt16; i++ {
		m[i] = true
	}

	//var primes []int

	for i := 2; i*i <= math.MaxInt16; i++ {
		if m[i] {
			for j := i * i; j < math.MaxInt16; j += i {
				m[j] = false
			}
		}
	}

	var primes []int

	for number, isPrime := range m {
		if isPrime {
			primes = append(primes, number)
		}
	}

	//sort.Sort(m)

	fmt.Println(primes)

	return 0, false
}

func factorial(n int) int {
	output := 1

	for i := 1; i < n; i++ {
		output *= n
	}

	return output
}
