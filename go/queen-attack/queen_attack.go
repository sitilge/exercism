package queenattack

import (
	"errors"
	"math"
	"strconv"
)

//CanQueenAttack checks if two queens can attack each other.
func CanQueenAttack(a string, b string) (bool, error) {
	qa, err := New(a)
	if err != nil {
		return false, err
	}

	qb, err := New(b)
	if err != nil {
		return false, err
	}

	if qa.row == qb.row && qa.column == qb.column {
		return false, errors.New("queens on the same spot")
	}

	if qa.row == qb.row || qa.column == qb.column {
		return true, nil
	}

	deltaRows := int(math.Abs(float64(qa.row) - float64(qb.row)))
	deltaCols := int(math.Abs(float64(qa.column) - (float64(qb.column))))

	if deltaRows == deltaCols {
		return true, nil
	}

	return false, nil
}

//Queen holds all the info about the row and column.
type Queen struct {
	row    rune
	column int
}

//New returns a new queen.
func New(queen string) (*Queen, error) {
	if len(queen) == 0 || len(queen) > 8 {
		return nil, errors.New("out of bounds string")
	}

	row := rune(queen[0])
	if row < 'a' || row > 'h' {
		return nil, errors.New("faulty row")
	}

	column, err := strconv.Atoi(string(queen[1]))
	if err != nil {
		return nil, err
	}

	if column < 1 || column > 8 {
		return nil, errors.New("faulty column")
	}

	return &Queen{
		row:    row,
		column: column,
	}, nil
}
