// Package house implements the Jack rhyming story recursively
package house

import (
	"fmt"
	"strings"
)

// Song returns all verses of our Jack house rhyming story
func Song() string {
	var output []string

	for i := 1; i <= 12; i++ {
		output = append(output, Verse(i))
	}

	return strings.Join(output, "\n\n")
}

// Verse constructs the verses of the Jack house rhyming story
func Verse(day int) string {
	return recurse(day, "")
}

func recurse(day int, input string) string {
	if day == 0 {
		return input
	}

	var p1, p2 string

	if input == "" {
		p1 = "This is"
	}

	switch day {
	case 1:
		if input != "" {
			p1 = "that lay in"
		}

		p2 = "the house that Jack built."
	case 2:
		if input != "" {
			p1 = "that ate"
		}

		p2 = "the malt"
	case 3:
		if input != "" {
			p1 = "that killed"
		}

		p2 = "the rat"
	case 4:
		if input != "" {
			p1 = "that worried"
		}

		p2 = "the cat"
	case 5:
		if input != "" {
			p1 = "that tossed"
		}

		p2 = "the dog"
	case 6:
		if input != "" {
			p1 = "that milked"
		}

		p2 = "the cow with the crumpled horn"
	case 7:
		if input != "" {
			p1 = "that kissed"
		}

		p2 = "the maiden all forlorn"
	case 8:
		if input != "" {
			p1 = "that married"
		}

		p2 = "the man all tattered and torn"
	case 9:
		if input != "" {
			p1 = "that woke"
		}

		p2 = "the priest all shaven and shorn"
	case 10:
		if input != "" {
			p1 = "that kept"
		}

		p2 = "the rooster that crowed in the morn"
	case 11:
		if input != "" {
			p1 = "that belonged to"
		}

		p2 = "the farmer sowing his corn"
	case 12:
		if input != "" {
			p1 = "the horse"
		}

		p2 = "the horse and the hound and the horn"
	}

	if input == "" {
		return recurse(day-1, fmt.Sprintf("%s %s", p1, p2))
	}

	return recurse(day-1, fmt.Sprintf("%s\n%s %s", input, p1, p2))
}
