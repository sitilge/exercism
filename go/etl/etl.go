package etl

import "strings"

//Transform one mapping into another.
func Transform(inputs map[int][]string) map[string]int {
	output := make(map[string]int)
	for value, letters := range inputs {
		for _, letter := range letters {
			letter := strings.ToLower(letter)
			output[letter] = value
		}
	}

	return output
}
