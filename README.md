# exercism

Solutions to https://exercism.io tasks.

## Notes on Go track

- acronym - has some regexp that might be handy sometime.
- bank-account - super important, using synchronization primitives like mutexes.
- clock - interesting that test cases used `reflect.DeepEqual`, task around time.
- crypto-square - took way too long.
- error-handling - probably, the most important task.
- flatten-array - super important, using raw interfaces.
- grains - working with bits.
- hamming - careful with UTF strings! When you do `for i := range someString` you are doing right and getting separate code points, but don't do `someString[i]` because it will get you the byte, not the rune. You can convert before like `[]rune(someString)`.
- matrix - very interesting, matrix parsing.
- parallel-letter-frequency - concurrency related, important. NOT WORKING, tests failing due to goroutines finishing randomly!
- reverse-string - although initially simple task, beware of using `len` vs `range` on strings, because they might be UTF encoded. `len` returns the number of bytes, while `range` iterates over the code points.
- robot-name - tests are using the `-tags` flag, which makes more tests to run. Don't forget to initialize channels with `make` instead of just defining e.g. `var ch chan int`. Be careful.
- run-length-encoding - it was easy by design but took some time, all those nitty-gritty things.
- sublist - easy, but I think I cheated.
- strain - interesting, using functions as arguments.
- tournament - custom sorting and output formatting. Check out the scanner for reading lines! Check out that custom sorting using sort.Slice!
- tree-building - interesting exercise, building any tree.
- twelve-days - recursion, was fun.